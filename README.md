Asteroids G12

UNE COSC101 / Trimester 1 2018

Authors: Josh Seehars, John Young and Andy Taylor

Game History:

Ateroids originally designed by Lyle Rains and Dominic Walsh, released by Atari. Inc. in 1971.
Simply, Asteroids is an 'arcade space shooter' game. A spaceship obejct is controlled by the player with the ability to shoot bullets, and move around an asteroid filled space.
Occasionally random flying saucers will appear throughout the virtual space.
The key object of the game is to shoot moving asteroids, and saucers.  Caution is needed not to collide with either, or death occurs.

G12 Game Development:
